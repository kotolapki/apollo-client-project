module.exports = {
  root: true,
  extends: '@react-native-community',
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  rules: {
    'max-len': [0, 120, 2],
    'prettier/prettier': ['error', { singleQuote: true }, { usePrettierrc: true }],
    'react-hooks/exhaustive-deps': 'warn',
  },
};
