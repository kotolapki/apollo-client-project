import React, {useState} from 'react';
import { useHistory } from 'react-router';
import { useForm } from 'react-hook-form';
import { SIGNUP_MUTATION, LOGIN_MUTATION } from '../../queries/queries';
import { useMutation } from '@apollo/client';
import { AUTH_TOKEN } from '../../constants';

const Login = () => {
  const [isLogin, setIsLogin] = useState(true);

  const history = useHistory();
  const {handleSubmit, register, getValues, formState} = useForm({defaultValues: {name: '', email: '', password: ''}});
  const {errors} = formState;

  const handleOnCompleted = (token) => {
    localStorage.setItem(AUTH_TOKEN, token);
    history.push('/');
  };

  const [login] = useMutation(LOGIN_MUTATION, {
    variables: {email: getValues().email, password: getValues().password}, 
    onCompleted: ({login}) => handleOnCompleted(login.token),
  });
  const [signup] = useMutation(SIGNUP_MUTATION, {
    variables: {name: getValues().name, email: getValues().email, password: getValues().password},
    onCompleted: ({signup}) => handleOnCompleted(signup.token),
  });

  const requiredMessage = 'this field is required';
  const emailPattern = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
  const minNameLengthMessage = 'min length is 3 symbols';
  const minPasswordLengthMessage = 'min length is 6 symbols';
  const emailPatternMessage = 'type exist email address';

  const onSubmit = () => {
    isLogin ? login() : signup();
  };

  return (
    <div>
      <h4 className="mv3">
        {isLogin ? 'Login' : 'Sign Up'}
      </h4>
      <div className="flex flex-column">
        {!isLogin && (
          <>
            <input
              style={{borderColor: errors.name ? 'red' : 'black'}}
              type="text"
              placeholder="Your name"
              {...register('name', {required: requiredMessage, minLength: {value: 3, message: minNameLengthMessage}})}
            />
            {errors.name && 
              (<div>
                <p style={{color: 'red'}}>{errors.name.message}</p>
              </div>)
            }
          </>
        )}
        <input
          style={{borderColor: errors.email ? 'red' : 'black'}}
          type="email"
          placeholder="Your email address"
          {...register('email', {required: requiredMessage ,pattern: {value: emailPattern, message: emailPatternMessage}})}
        />
        {errors.email && 
          (<div>
            <p style={{color: 'red'}}>{errors.email.message}</p>
          </div>)
        }
        <input
          style={{borderColor: errors.password ? 'red' : 'black'}}
          type="password"
          placeholder={isLogin ? 'Your password' : 'Choose a safe password'}
          {...register('password', {required: requiredMessage, minLength: {value: 6, message: minPasswordLengthMessage}})}
        />
        {errors.password && 
          (<div>
            <p style={{color: 'red'}}>{errors.password.message}</p>
          </div>)
        }
      </div>
      <div className="flex mt3">
        <button
          className="pointer mr2 button"
          onClick={handleSubmit(onSubmit)}
        >
          {isLogin ? 'login' : 'create account'}
        </button>
        <button
          className="pointer button"
          onClick={() => setIsLogin(!isLogin)}
        >
          {isLogin
            ? 'need to create an account?'
            : 'already have an account?'}
        </button>
      </div>
    </div>
  );
};

export default Login;