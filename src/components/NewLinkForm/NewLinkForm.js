import { useMutation } from '@apollo/client';
import React from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router';
import { CREATE_LINK_MUTATION, FEED_QUERY } from '../../queries/queries';
import { LINKS_PER_PAGE } from '../../constants';

const NewLinkForm = () => {
  const history = useHistory();
  const {handleSubmit, register} = useForm();
  const [createLink] = useMutation(CREATE_LINK_MUTATION, {
    update: (cache, { data: { post } }) => {
      const take = LINKS_PER_PAGE;
      const skip = 0;
      const orderBy = { createdAt: 'desc' };

      const data = cache.readQuery({
        query: FEED_QUERY,
        variables: {
          take,
          skip,
          orderBy
        }
      });

      cache.writeQuery({
        query: FEED_QUERY,
        data: {
          feed: {
            links: [post, ...data.feed.links]
          }
        },
        variables: {
          take,
          skip,
          orderBy
        }
      });
    },
    onCompleted: () => history.push('/new/1')
  });


  const onSubmit = (values) => {
    createLink({variables: {description: values.description, url: values.url}});
  }

  return (
    <form  className="flex flex-column mt3" onSubmit={handleSubmit(onSubmit)}>
      <input className="mb2" type='text' {...register('description')} />
      <input className="mb2" type='text' {...register('url')} />
      <button type="submit">Submit</button>
    </form>
  );
};

export default NewLinkForm;