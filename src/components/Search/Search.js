import React from 'react';
import { useLazyQuery } from '@apollo/client';
import Link from '../Link';
import { useForm } from 'react-hook-form';
import {FEED_SEARCH_QUERY} from '../../queries/queries';

const Search = () => {
  const {register, handleSubmit} = useForm();

  const [executeSearch, {data}] = useLazyQuery(FEED_SEARCH_QUERY);

  const handleSearchOnPress = (values) => {
    executeSearch({variables: {filter: values.search}});
  };

  return (
    <>
      <div>
        Search
        <input
          type="text"
          {...register('search')}
        />
        <button onClick={handleSubmit(handleSearchOnPress)}>OK</button>
      </div>
      {data &&
        data.feed.links.map((link, index) => (
          <Link key={link.id} link={link} index={index} />
        ))}
    </>
  );
};

export default Search;